// instantiate the Phaser game scene
//
let gameScene = new Phaser.Scene("Game");
const WIDTH = 640;
const HEIGHT = 360;

gameScene.preload = function() {
  // load images
  this.load.image("background", "../assets/background.png");
  this.load.image("player", "../assets/player.png");
  this.load.image("enemy", "../assets/dragon.png");
};

gameScene.create = function() {
  this.maxSize = false;
  this.bg = this.add.sprite(0, 0, "background");
  this.bg.setPosition(WIDTH / 2, HEIGHT / 2);
  console.log(this.bg);
  this.player = this.add.sprite(60, HEIGHT / 2, "player");
  this.player.setScale(0.5);
  let gameW = this.sys.game.config.width;
  let gameH = this.sys.game.config.height;
  console.log(gameW);
  console.log(gameH);
  console.log(this);
  // create enemy
  this.enemy1 = this.add.sprite(250, 180, "enemy");
  this.enemy1.scaleX = 2;
  this.enemy1.scaleY = 2;

  this.enemy2 = this.add.sprite(500, 180, "enemy");
  this.enemy2.displayWidth = 100;
  this.enemy2.displayHeight = 90;

  // up side down
  //enemy1.flipX = true;
  //enemy1.flipY = true;

  // facing player
  this.enemy1.flipX = true;
  this.enemy1.flipY = false;

  this.enemy1.rotation = Math.PI / 4;
  this.enemy2.setRotation(Math.PI / 6);
};

gameScene.update = function() {
  this.enemy1.angle += 1;
  console.log(this.maxSize);
  if (!this.maxSize) {
    console.log("enlarge");
    this.player.scaleX += 0.01;
    this.player.scaleY += 0.01;
    console.log(this.player.scaleX);
    if (this.player.scaleX > 2) {
      this.maxSize = true;
    }
  }

  if (this.maxSize) {
    console.log("shrink");
    this.player.scaleX = this.player.scaleX - 0.01;
    this.player.scaleY = this.player.scaleY - 0.01;
    // console.log(this.player.scaleX);
    if (this.player.scaleX < 0.6) {
      console.log("SET TO FALSE !");
      this.maxSize = false;
    }
  }
};

let config = {
  type: Phaser.Auto,
  width: WIDTH,
  height: HEIGHT,
  scene: gameScene
};

let game = new Phaser.Game(config);
